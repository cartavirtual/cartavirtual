
package cartavirtual.cartavirtual;

        import android.support.v4.app.FragmentActivity;
        import android.os.Bundle;

        import com.google.android.gms.maps.CameraUpdateFactory;
        import com.google.android.gms.maps.GoogleMap;
        import com.google.android.gms.maps.OnMapReadyCallback;
        import com.google.android.gms.maps.SupportMapFragment;
        import com.google.android.gms.maps.model.LatLng;
        import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(6.2417656, -75.5898585), 16));


        LatLng buelevar = new LatLng(6.2417656, -75.5898585);
        mMap.addMarker(new MarkerOptions().position(buelevar).title("Marker in buelevar upb"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(buelevar));

        LatLng derecho = new LatLng(6.243328, -75.590558);
        mMap.addMarker(new MarkerOptions().position(derecho).title("Marker in Derecho"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(derecho));
    }
}


