package cartavirtual.cartavirtual;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class EntradaActivity extends ListActivity {

    private MyAdapter mAdapter=null;


    public class Node{
        public String mTitle;
        public String mDescription;
        public Integer mImageResource;

    }

    static ArrayList<Node> mArray = new ArrayList<Node>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setData();

        mAdapter = new MyAdapter(this);
        setListAdapter(mAdapter);


    }




    public static class MyAdapter extends BaseAdapter{


        private Context mContext;

        public MyAdapter(Context c){
            mContext=c;
        }


        @Override
        public int getCount() {
            return mArray.size();
        }

        @Override
        public Object getItem(int i) {
            return mArray.get(i);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View view1 =null;
            if(view==null){

                LayoutInflater inflater = (LayoutInflater)mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view1=inflater.inflate(R.layout.activity_entrada,null);
            }
            else{
                view1=view;
            }

            ImageView img=(ImageView)view1.findViewById(R.id.image);
            img.setImageDrawable(mContext.getResources().getDrawable(mArray.get(i).mImageResource));

            TextView tTitle=(TextView) view1.findViewById(R.id.title);
            tTitle.setText(mArray.get(i).mTitle);

            TextView Tdescription= (TextView) view1.findViewById(R.id.description);
            Tdescription.setText(mArray.get(i).mDescription);

            return view1;
        }
    }

    private void setData(){

        mArray.clear();

        Node mynode = new Node();

        //Empanadas
        mynode.mTitle=this.getResources().getString(R.string.titleEmpanadas);
        mynode.mDescription=this.getResources().getString(R.string.descriptionEmpanadas);
        mynode.mImageResource=R.drawable.empanadas;

        mArray.add(mynode);


        Node mynode2 = new Node();

        //Patacones
        mynode2.mTitle=this.getResources().getString(R.string.titlePatacones);
        mynode2.mDescription=this.getResources().getString(R.string.descriptionPatacones);
        mynode2.mImageResource=R.drawable.patacones;

        mArray.add(mynode2);



        Node mynode3 = new Node();

        //Bebidas
        mynode3.mTitle=this.getResources().getString(R.string.titlePalitos);
        mynode3.mDescription=this.getResources().getString(R.string.descriptionPalitos);
        mynode3.mImageResource=R.drawable.palitos;

        mArray.add(mynode3);



        Node mynode4 = new Node();

        //Postres
        mynode4.mTitle=this.getResources().getString(R.string.titlePicada);
        mynode4.mDescription=this.getResources().getString(R.string.descriptionPicada);
        mynode4.mImageResource=R.drawable.picada;

        mArray.add(mynode4);


    }
}
