package cartavirtual.cartavirtual;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CartaActivity extends ListActivity {

    private MyAdapter mAdapter=null;


    public class Node{
        public String mTitle;
        public String mDescription;
        public Integer mImageResource;

    }

     static ArrayList<Node> mArray = new ArrayList<Node>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setData();

        mAdapter = new MyAdapter(this);
        setListAdapter(mAdapter);


    }
    void VerEntradas(View v){//el view solo es una referencia a la vista
        Intent intencion= new Intent(this ,EntradaActivity.class); //se especifica la clase que hace la solicitud y la clase que la recibe
        startActivity(intencion);}

    void VerPlatos(View v){//el view solo es una referencia a la vista
        Intent intencion= new Intent(this ,PlatoActivity.class); //se especifica la clase que hace la solicitud y la clase que la recibe
        startActivity(intencion);}

    void VerBebidas(View v){//el view solo es una referencia a la vista
        Intent intencion= new Intent(this ,BebidasActivity.class); //se especifica la clase que hace la solicitud y la clase que la recibe
        startActivity(intencion);}

    void VerPostres(View v){//el view solo es una referencia a la vista
        Intent intencion= new Intent(this ,PostresActivity.class); //se especifica la clase que hace la solicitud y la clase que la recibe
        startActivity(intencion);}

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        if(position==0) {
            Intent i = new Intent(this, EntradaActivity.class);
            startActivity(i);
        } else if (position==1) {
            Intent i = new Intent(this, PlatoActivity.class);
            startActivity(i);
        }
        else if (position==2){
            Intent i = new Intent(this, BebidasActivity.class);
            startActivity(i);
        }
        else{
            Intent i = new Intent(this, PostresActivity.class);
            startActivity(i);
        }

    }

    public static class MyAdapter extends BaseAdapter{


        private Context mContext;

        public MyAdapter(Context c){
            mContext=c;
        }


        @Override
        public int getCount() {
            return mArray.size();
        }

        @Override
        public Object getItem(int i) {
            return mArray.get(i);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View view1 =null;
            if(view==null){

                LayoutInflater inflater = (LayoutInflater)mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view1=inflater.inflate(R.layout.activity_carta,null);
            }
            else{
                view1=view;
            }

            ImageView img=(ImageView)view1.findViewById(R.id.image);
            img.setImageDrawable(mContext.getResources().getDrawable(mArray.get(i).mImageResource));

            TextView tTitle=(TextView) view1.findViewById(R.id.title);
            tTitle.setText(mArray.get(i).mTitle);

            TextView Tdescription= (TextView) view1.findViewById(R.id.description);
            Tdescription.setText(mArray.get(i).mDescription);

            return view1;
        }
    }

    private void setData(){

        mArray.clear();

        Node mynode = new Node();

        //Entradas
        mynode.mTitle=this.getResources().getString(R.string.titleEntradas);
        mynode.mDescription=this.getResources().getString(R.string.descriptionEntradas);
        mynode.mImageResource=R.drawable.entradas;

        mArray.add(mynode);


        Node mynode2 = new Node();

        //Platos
        mynode2.mTitle=this.getResources().getString(R.string.titlePlatos);
        mynode2.mDescription=this.getResources().getString(R.string.descriptionPlatos);
        mynode2.mImageResource=R.drawable.platos;

        mArray.add(mynode2);



        Node mynode3 = new Node();

        //Bebidas
        mynode3.mTitle=this.getResources().getString(R.string.titleBebidas);
        mynode3.mDescription=this.getResources().getString(R.string.descriptionBebidas);
        mynode3.mImageResource=R.drawable.bebidas;

        mArray.add(mynode3);



        Node mynode4 = new Node();

        //Postres
        mynode4.mTitle=this.getResources().getString(R.string.titlePostres);
        mynode4.mDescription=this.getResources().getString(R.string.descriptionPostres);
        mynode4.mImageResource=R.drawable.helado;

        mArray.add(mynode4);


    }
}
