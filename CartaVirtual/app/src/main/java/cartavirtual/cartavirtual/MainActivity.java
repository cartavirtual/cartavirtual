package cartavirtual.cartavirtual;

import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import  android.content.Intent;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String whatsApp ="com.whatsapp";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    void ConsultarUbicacion(View v){//el view solo es una referencia a la vista
        Intent intencion= new Intent(this ,MapsActivity.class); //se especifica la clase que hace la solicitud y la clase que la recibe
        startActivity(intencion);

    }
    void verCarta(View v){//el view solo es una referencia a la vista
        Intent intencion= new Intent(this ,CartaActivity.class); //se especifica la clase que hace la solicitud y la clase que la recibe
        startActivity(intencion);

    }
    void reservaMesa(View v){//el view solo es una referencia a la vista

        PackageManager pm = getPackageManager();// se crea un objeto que
        Intent intent= pm.getLaunchIntentForPackage(whatsApp);// me llena el intent con ese paquete
        startActivity(intent);

    }
    void calificarServicio(View v){//el view solo es una referencia a la vista
        /*Intent intencion= new Intent(this ,servicioActivity.class); //se especifica la clase que hace la solicitud y la clase que la recibe
        startActivity(intencion);*/

    }

}
